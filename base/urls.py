from django.urls import path, include
from . import views
from django.conf import settings
from django.conf.urls.static import static

app_name = 'base'

urlpatterns = [
    path('', views.home, name='home'),
    path('files/<str:choice>', views.files, name='files'),
    path('profile/', views.profile, name='profile'),
    path('blog/<int:choice>', views.blog, name='blog'),
    path('post_detail/<slug:slug>/', views.post_detail, name='post_detail'),
]